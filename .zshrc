# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# check name of PC and assign work environment var
if [[ $(uname -n) == "kubag-pc" ]]; then
    WORK_ENV="home"
else
    WORK_ENV="work"
fi

# Enable colors and change prompt:
autoload -Uz colors && colors

# History in cache directory:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history

# Basic auto/tab complete:
autoload -Uz compinit
zstyle ':completion:*' menu select
# Auto complete with case insenstivity
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.
source $HOME/.config/zsh/plugins/completion.zsh

# Enable searching through history
# bindkey '^R' history-incremental-pattern-search-backward
# disabled, using fzf now.

# FZF
if [[ $WORK_ENV == "home" ]]; then
    source /usr/share/fzf/key-bindings.zsh
    source /usr/share/fzf/completion.zsh
else
    source /usr/share/doc/fzf/examples/key-bindings.zsh
    source /usr/share/doc/fzf/examples/completion.zsh
fi
lb () {
    FZF_DEFAULT_OPTS="--height ${FZF_TMUX_HEIGHT:-40%} $FZF_DEFAULT_OPTS -n2..,.. --tiebreak=index --bind=ctrl-r:toggle-sort $FZF_CTRL_R_OPTS --query=${(qqq)LBUFFER} +m"
    git checkout $(git branch -a | grep -v remotes | tr "*" " "| fzf)
}
rb () {
    FZF_DEFAULT_OPTS="--height ${FZF_TMUX_HEIGHT:-40%} $FZF_DEFAULT_OPTS -n2..,.. --tiebreak=index --bind=ctrl-r:toggle-sort $FZF_CTRL_R_OPTS --query=${(qqq)LBUFFER} +m"
	git checkout $(git branch -a | grep remotes | grep -o -P "(?<=\s\sremotes\/origin\/)[a-zA-Z0-9-_]*"|fzf)
}
# setting fzf theme
source $HOME/.config/zsh/config/fzf.zsh

# vi mode
bindkey -v
export KEYTIMEOUT=1

# Edit line in vim buffer ctrl-v
autoload edit-command-line; zle -N edit-command-line
bindkey '^V' edit-command-line
# Enter vim buffer from normal mode
autoload -U edit-command-line && zle -N edit-command-line && bindkey -M vicmd "^v" edit-command-line

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'left' vi-backward-char
bindkey -M menuselect 'down' vi-down-line-or-history
bindkey -M menuselect 'up' vi-up-line-or-history
bindkey -M menuselect 'right' vi-forward-char
# Fix backspace bug when switching modes
bindkey "^?" backward-delete-char

# Delete deletes next char
bindkey "^[[3~" delete-char
# Jump to beggining and end of line
bindkey "^a" vi-beginning-of-line
bindkey "^e" vi-end-of-line

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select

# ci", ci', ci`, di", etc
autoload -U select-quoted
zle -N select-quoted
for m in visual viopp; do
  for c in {a,i}{\',\",\`}; do
    bindkey -M $m $c select-quoted
  done
done

# ci{, ci(, ci<, di{, etc
autoload -U select-bracketed
zle -N select-bracketed
for m in visual viopp; do
  for c in {a,i}${(s..)^:-'()[]{}<>bB'}; do
    bindkey -M $m $c select-bracketed
  done
done
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
precmd() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Prompt (disabled, using starship)
# fpath=($HOME/.config/zsh/prompts $fpath)
# autoload -Uz purification
# purification

# Directory stack
setopt AUTO_PUSHD           # Push the current directory visited on the stack.
setopt PUSHD_IGNORE_DUPS    # Do not store duplicates in the stack.
setopt PUSHD_SILENT         # Do not print the directory stack after pushd or popd.

alias d='dirs -v'
for index ({1..9}) alias "$index"="cd +${index}"; unset index

# Archive extraction
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Aliases

# pacman and yay
alias pacsyu='sudo pacman -Syyu'                 # update only standard pkgs
alias yaysua="yay -Sua"                          # update only AUR pkgs
alias yaysyu="yay -Syu"                          # update standard pkgs and AUR pkgs
alias unlock="sudo rm /var/lib/pacman/db.lck"    # remove pacman lock
alias cleanup='sudo pacman -Rns $(pacman -Qtdq)' # remove orphaned packages

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT -I ".git" --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# shutdown or reboot
alias ssn="shutdown now"
alias sr="reboot"

# get error messages from journalctl
alias jctl="journalctl -p 3 -xb"

alias zshconfig="nvim ~/.zshrc"

# alias ohmyzsh="mate ~/.oh-my-zsh"
alias dc="docker-compose"
alias dm="docker-machine"
alias ev="expressvpn"
alias tf="terraform"
# Dotfiles repo access
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
# Vimwiki repo access
alias vw='/usr/bin/git --git-dir=$HOME/vimwiki/.git/ --work-tree=$HOME/vimwiki/'

# Rust binaries
export PATH="$HOME/.cargo/bin:$PATH"

# clipmenu settings
export CM_SELECTIONS="clipboard"  # Only save copied things, not selected ones

# NVM
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Adding function path dir
fpath+=${ZDOTDIR:-~}/.zsh_functions

# Home stuff

if [[ $WORK_ENV == "home" ]]; then
    export PATH="$HOME/.emacs.d/bin:$PATH"

    # Hashes
    hash -d kb="$HOME/qmk_firmware/keyboards/planck/keymaps/j-godl"
fi

# Ubuntu stuff

alias aptu="sudo apt update && sudo apt upgrade"

# Work stuff

if [[ $WORK_ENV == "work" ]]; then
    export PATH="$HOME/.tfenv/bin:$PATH"

    export PATH="$HOME/.poetry/bin:$PATH"

    # Dhub docker exports
    export CLOUDSDK_PYTHON=/usr/bin/python
    export LD_LIBRARY_PATH=/usr/local/lib

    # Constants var
    export DWH_CONSTANTS_PATH="$HOME/repos/logistics-airflow/dags/mkt/configuration/yaml/dwh_constants/dwh_constants.yaml"

    # Aliases
    alias dhub="cd $HOME/repos/datahub-airflow && AIRFLOW_BUSINESS_UNIT="mkt" docker compose -f $HOME/repos/datahub-airflow/docker-compose.yml up webserver"
    alias dhub-scheduler="cd $HOME/repos/datahub-airflow && AIRFLOW_BUSINESS_UNIT="mkt" docker compose -f $HOME/repos/datahub-airflow/docker-compose-scheduler.yml up"
    alias dhub-test="docker compose -f $HOME/repos/datahub-airflow/docker-compose.yml exec webserver"
    alias mkt-airflow="docker-compose -f $HOME/repos/mkt-airflow/local_env/docker-compose.yml"

    # Hashes
    hash -d dhub="$HOME/repos/datahub-airflow"
    # hash -d crm="$HOME/repos/mkt-crm-feed"
    # hash -d data="$HOME/repos/mkt-data"
    hash -d infra="$HOME/repos/mkt-infra-tf"
    # hash -d ml="$HOME/repos/mkt-ml"
    hash -d reorder="$HOME/repos/mkt-reorder"

    # Voutopia
    hash -d vou="$HOME/repos/mkt-voutopia"
    hash -d vservices="$HOME/repos/mkt-voutopia-services"
    hash -d vfeed="$HOME/repos/mkt-frequency-feed"
    hash -d vinfra="$HOME/repos/mkt-incentives-infra"

    # Qubik
    hash -d q="$HOME/repos/dsci-qubik"
fi

# direnv app support
if [ -f /usr/bin/direnv ]; then eval "$(direnv hook zsh)"; fi;

# export GOOGLE_APPLICATION_CREDENTIALS="/home/j.godlewski/.gcp-credentials/mkt-tech-29912649fa14.json"

# pyenv support
if [[ $WORK_ENV == "work" ]]; then
    export PATH="$HOME/.pyenv/bin:$PATH"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
else
    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$PYENV_ROOT/bin:$PATH"
    eval "$(pyenv init --path)"
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi

# starship prompt init
eval "$(starship init zsh)"

# Syntax higlighting plugin
if [[ $WORK_ENV == "work" ]]; then
    source $HOME/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
else
    source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi
