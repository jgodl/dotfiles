# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import socket
import subprocess
from time import time
from pathlib import Path
import pwd
import re
import logging

from libqtile.config import Key, Screen, Group, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget
from libqtile import hook
from Xlib import display as xdisplay

from typing import List  # noqa: F401


logger = logging.getLogger(__name__)


def is_running(process):
    s = subprocess.Popen(["ps", "axw"], stdout=subprocess.PIPE)
    for x in s.stdout:
        if re.search(process, x.decode()):
            return True
    return False


def execute_once(process):
    if not is_running(process):
        print(f"executing {process}")
        return subprocess.Popen(process.split())


@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser("~/.config/qtile/autostart.sh")
    subprocess.call([home])


@hook.subscribe.screen_change
def restart_on_randr(qtile, ev):
    qtile.cmd_restart()


def get_num_monitors():
    num_monitors = 0
    try:
        display = xdisplay.Display()
        screen = display.screen()
        resources = screen.root.xrandr_get_screen_resources()

        for output in resources.outputs:
            monitor = display.xrandr_get_output_info(output, resources.config_timestamp)
            preferred = False
            if hasattr(monitor, "preferred"):
                preferred = monitor.preferred
            elif hasattr(monitor, "num_preferred"):
                preferred = monitor.num_preferred
            if preferred:
                num_monitors += 1
    except Exception as e:
        # always setup at least one monitor
        return 1
    else:
        return num_monitors


num_monitors = get_num_monitors()

mod = "mod4"


def screenshot(save=True, copy=True):
    def f(qtile):
        path = Path.home() / "Pictures"
        path /= f"screenshot_{str(int(time() * 100))}.png"
        shot = subprocess.run(["maim"], stdout=subprocess.PIPE)

        if save:
            with open(path, "wb") as sc:
                sc.write(shot.stdout)

        if copy:
            subprocess.run(
                ["xclip", "-selection", "clipboard", "-t", "image/png"],
                input=shot.stdout,
            )

    return f


def backlight(action):
    def f(qtile):
        brightness = int(
            float(subprocess.run(["light", "-G"], stdout=subprocess.PIPE).stdout)
        )
        if brightness != 1 or action != "U":
            if (brightness > 49 and action == "U") or (
                brightness > 39 and action == "A"
            ):
                subprocess.run(["light", f"-{action}", "10"])
            else:
                subprocess.run(["light", f"-{action}", "1"])

    return f


user_pc = pwd.getpwuid(os.getuid()).pw_name

pc_spec = {
    "kubag": {"term": "st"},
    "j.godlewski": {"term": "st"},
}

spec = pc_spec[user_pc]

keys = [
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down()),
    Key([mod], "j", lazy.layout.up()),
    # Move windows up or down in current stack
    Key([mod, "control"], "k", lazy.layout.shuffle_down()),
    Key([mod, "control"], "j", lazy.layout.shuffle_up()),
    Key(
        [mod, "shift"],
        "l",
        lazy.layout.grow(),  # Grow size of current window (XmonadTall)
        lazy.layout.increase_nmaster(),  # Increase number in master pane (Tile)
    ),
    Key(
        [mod, "shift"],
        "h",
        lazy.layout.shrink(),  # Shrink size of current window (XmonadTall)
        lazy.layout.decrease_nmaster(),  # Decrease number in master pane (Tile)
    ),
    Key(
        [mod],
        "n",
        lazy.layout.normalize(),  # Restore all windows to default size ratios
    ),
    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next()),
    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate()),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split()),
    # Screens
    Key([mod], "a", lazy.to_screen(0)),  # Keyboard focus screen(0)
    Key([mod], "s", lazy.to_screen(1)),  # Keyboard focus screen(1)
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout()),
    Key([mod], "w", lazy.window.kill()),
    Key([mod, "control"], "r", lazy.restart()),
    Key([mod, "control"], "q", lazy.shutdown()),
    Key([mod], "r", lazy.spawncmd()),
    # Audio
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -c 0 -q set Master 2dB+")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -c 0 -q set Master 2dB-")),
    Key([], "XF86AudioMute", lazy.spawn("amixer -c 0 -q set Master toggle")),
    # Screen
    # Key([], "XF86MonBrightnessUp", lazy.function(backlight("inc"))),
    # Key([], "XF86MonBrightnessDown", lazy.function(backlight("dec"))),
    Key([], "XF86MonBrightnessUp", lazy.function(backlight("A"))),
    Key([], "XF86MonBrightnessDown", lazy.function(backlight("U"))),
    # Screenshots
    Key([], "Print", lazy.function(screenshot())),
    Key(["control"], "Print", lazy.spawn("deepin-screenshot")),
    # Applications
    Key([mod], "Return", lazy.spawn(spec["term"])),
]

# Colors

colors = [
    ["#282a36", "#282a36"],  # panel background
    ["#434758", "#434758"],  # background for current screen tab
    ["#ffffff", "#ffffff"],  # font color for group names
    ["#ff5555", "#ff5555"],  # background color for layout widget
    ["#000000", "#000000"],  # background for other screen tabs
    ["#A77AC4", "#A77AC4"],  # dark green gradiant for other screen tabs
    ["#50fa7b", "#50fa7b"],  # background color for network widget
    ["#7197E7", "#7197E7"],  # background color for pacman widget
    ["#9AEDFE", "#9AEDFE"],  # background color for cmus widget
    ["#000000", "#000000"],  # background color for clock widget
    ["#434758", "#434758"],
]

# Groups

group_names = [
    ("WWW", {"layout": "monadtall"}),
    ("DEV", {"layout": "monadtall"}),
    ("ETC", {"layout": "monadtall"}),
    ("MUS", {"layout": "monadtall"}),
    # ("VBOX", {'layout': 'max'}),
    # ("CHAT", {'layout': 'max'}),
    # ("MUS", {'layout': 'max'}),
    # ("VID", {'layout': 'max'}),
    # ("GFX", {'layout': 'floating'})
]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(
        Key([mod], str(i), lazy.group[name].toscreen())
    )  # Switch to another group
    keys.append(
        Key([mod, "shift"], str(i), lazy.window.togroup(name))
    )  # Send current window to another group

layout_theme = {
    "border_width": 2,
    "margin": 4,
    "border_focus": "AD69AF",
    "border_normal": "1D2330",
}

layouts = [
    # layout.Bsp(**layout_theme),
    # layout.Stack(stacks=2, **layout_theme),
    # layout.Columns(**layout_theme),
    # layout.RatioTile(**layout_theme),
    # layout.VerticalTile(**layout_theme),
    # layout.Tile(shift_windows=True, **layout_theme),
    # layout.Matrix(**layout_theme),
    # layout.Zoomy(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.TreeTab(
        font="Ubuntu",
        fontsize=10,
        sections=["FIRST", "SECOND"],
        section_fontsize=11,
        bg_color="141414",
        active_bg="90C435",
        active_fg="000000",
        inactive_bg="384323",
        inactive_fg="a0a0a0",
        padding_y=5,
        section_top=10,
        panel_width=320,
        **layout_theme,
    ),
    layout.Floating(**layout_theme),
]

updates_widgets = []

if user_pc == "kubag":
    updates_widgets = [
        widget.TextBox(
            font="Ubuntu Light",
            text=" ⟳",
            padding=5,
            foreground=colors[2],
            background=colors[5],
            fontsize=14,
        ),
        widget.Pacman(
            font="Ubuntu Light",
            execute="st",
            update_interval=1800,
            foreground=colors[2],
            background=colors[5],
            fontsize=14,
        ),
        widget.TextBox(
            font="Ubuntu Light",
            text="Updates",
            padding=5,
            foreground=colors[2],
            background=colors[5],
            fontsize=14,
        ),
    ]

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())
widgets_list = [
    widget.Sep(linewidth=0, padding=6, foreground=colors[2], background=colors[0]),
    widget.GroupBox(
        font="Ubuntu Bold",
        fontsize=9,
        margin_y=0,
        margin_x=0,
        padding_y=5,
        padding_x=5,
        borderwidth=1,
        active=colors[2],
        inactive=colors[2],
        rounded=False,
        highlight_method="block",
        this_current_screen_border=colors[5],
        this_screen_border=colors[1],
        other_current_screen_border=colors[0],
        other_screen_border=colors[0],
        foreground=colors[2],
        background=colors[0],
    ),
    widget.Prompt(
        prompt=prompt,
        font="Ubuntu Mono",
        padding=10,
        foreground=colors[3],
        background=colors[1],
    ),
    widget.Sep(linewidth=0, padding=10, foreground=colors[2], background=colors[0]),
    widget.WindowName(
        font="Ubuntu",
        fontsize=11,
        foreground=colors[5],
        background=colors[0],
        padding=5,
    ),
    widget.Systray(
        font="Ubuntu Light",
        padding=5,
        foreground=colors[2],
        background=colors[7],
        fontsize=14,
    ),
    widget.TextBox(
        font="Ubuntu Light",
        text=" 🗲",
        padding=5,
        foreground=colors[2],
        background=colors[5],
        fontsize=14,
    ),
    widget.Battery(
        font="Ubuntu Light",
        padding=5,
        foreground=colors[2],
        background=colors[5],
        fontsize=14,
        format="{char} {percent:2.0%}",
    ),
    widget.TextBox(
        font="Ubuntu Light",
        text=" ☵",
        padding=5,
        foreground=colors[2],
        background=colors[7],
        fontsize=14,
    ),
    widget.CurrentLayout(
        font="Ubuntu Light",
        foreground=colors[2],
        background=colors[7],
        padding=5,
        fontsize=14,
    ),
    *updates_widgets,
    widget.TextBox(
        font="Ubuntu Light",
        text=" 🔊",
        background=colors[7],
        foreground=colors[2],
        padding=0,
        fontsize=14,
    ),
    widget.Volume(
        foreground=colors[2],
        background=colors[7],
        font="Ubuntu Light",
        padding=5,
        fontsize=14,
    ),
    widget.TextBox(
        font="Ubuntu Light",
        text=" 🕒",
        foreground=colors[2],
        background=colors[5],
        padding=5,
        fontsize=14,
    ),
    widget.Clock(
        font="Ubuntu Light",
        foreground=colors[2],
        background=colors[5],
        format="%A, %Y-%m-%d - %H:%M:%S",
        fontsize=14,
    ),
    widget.Sep(linewidth=0, padding=5, foreground=colors[0], background=colors[5]),
]

screens = [Screen(top=bar.Bar(widgets=widgets_list, opacity=0.95, size=20))]

if num_monitors > 1:
    for m in range(num_monitors - 1):
        screens.append(top=bar.Bar(widgets=widgets_list, opacity=0.95, size=20))

# Drag floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        {"wmclass": "confirm"},
        {"wmclass": "dialog"},
        {"wmclass": "download"},
        {"wmclass": "error"},
        {"wmclass": "file_progress"},
        {"wmclass": "notification"},
        {"wmclass": "splash"},
        {"wmclass": "toolbar"},
        {"wmclass": "confirmreset"},  # gitk
        {"wmclass": "makebranch"},  # gitk
        {"wmclass": "maketag"},  # gitk
        {"wname": "branchdialog"},  # gitk
        {"wname": "pinentry"},  # GPG key password entry
        {"wmclass": "ssh-askpass"},  # ssh-askpass
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
