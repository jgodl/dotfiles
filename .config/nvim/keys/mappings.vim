" g Leader key
let mapleader=" "
nnoremap <Space> <Nop>

" TAB in general mode will move to text buffer
nnoremap <silent> <TAB> :bnext<CR>
" SHIFT-TAB will go back
nnoremap <silent> <S-TAB> :bprevious<CR>

" delete buffer with CTRL+C
map <leader>bd :bp<bar>sp<bar>bn<bar>bd<CR>

