let g:vimwiki_list = [
        \ {
            \ 'path': '~/vimwiki/',
            \ 'syntax': 'markdown',
            \ 'ext': '.md',
            \ 'links_space_char': '_',
            \ 'nested_syntaxes': {'python': 'python', 'java': 'java'},
        \ }
    \ ]

" Remaping going to next/previous link in wiki, standard is Tab
nmap <PageDown> <Plug>VimwikiNextLink
nmap <PageUp> <Plug>VimwikiPrevLink

