" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
    silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall | source $HOME/.config/nvim/init.vim
endif

call plug#begin('~/.config/nvim/autoload/plugged')

    " Surround
    Plug 'tpope/vim-surround'

    " Commentary
    Plug 'tpope/vim-commentary'

    " Themes
    Plug 'gruvbox-community/gruvbox'

    " Python Syntax
    Plug 'vim-python/python-syntax'

    " Syntax highliting
    Plug 'sheerun/vim-polyglot'

    " Status Line
    Plug 'vim-airline/vim-airline'

    " Ranger support
    " Plug 'kevinhwang91/rnvimr', {'do': 'make sync'}
    Plug 'kevinhwang91/rnvimr'

    " Dir tree
    Plug 'preservim/nerdtree'

    " Intellisense
    Plug 'neoclide/coc.nvim', {'branch': 'release'}

    " fzf
    Plug 'junegunn/fzf', {'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'
    Plug 'airblade/vim-rooter'

    " Git
    Plug 'mhinz/vim-signify'
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-rhubarb'
    Plug 'junegunn/gv.vim'

    " Highliting trailing whitespaces
    Plug 'bronson/vim-trailing-whitespace'

    " Coloring color codes
    Plug 'norcalli/nvim-colorizer.lua'

    " Vim wiki
    Plug 'vimwiki/vimwiki'

    " md preview
    Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

call plug#end()

" Automatically install missing plugins on startup
autocmd VimEnter *
    \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
    \|   PlugInstall --sync | q
    \| endif

" CoC, fzf, fugitive, nerdtree, vim-commentary, vim-surround
